# Time Entry Change Notifier

Time Entry Change Notifier is a Redmine plugin that notifies time
entry change by e-mail.

## How to install

You need to apply a patch to Redmine. The patch is for adding support
for `call_hook` in mailer.

For Redmine 4.0.x:

```bash
curl https://github.com/kou/redmine/compare/4.0-stable...4.0-stable-hooks-in-mailer.diff | patch -p1
```

For Redmine 5.0.x:

```bash
curl https://github.com/kou/redmine/compare/5.0-stable...5.0-stable-hooks-in-mailer.diff | patch -p1
```

For Redmine trunk:

```bash
curl http://www.redmine.org/attachments/download/24286/support-hooks-in-mailer-for-r18899-3.patch | patch -p0
```

Install this plugin:

```bash
cd redmine
git clone https://gitlab.com/redmine-plugin-time-entry-change-notifier/redmine-plugin-time-entry-change-notifier.git plugins/time_entry_change_notifier
```

Restart Redmine.

## Authors

  * ClearCode Inc.

## License

GPL 2 or later. See LICENSE for details.
