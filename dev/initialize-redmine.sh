#!/bin/bash

set -exu

bin/rails db:drop || true
bin/rails generate_secret_token
bin/rails db:create
bin/rails db:migrate
bin/rails redmine:load_default_data REDMINE_LANG=en
bin/rails redmine:plugins:migrate
bin/rails runner '
u = User.find(1)
u.password = u.password_confirmation = "adminadmin"
u.must_change_passwd = false
u.save!
'
