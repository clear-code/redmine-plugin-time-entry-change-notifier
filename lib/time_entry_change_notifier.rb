module TimeEntryChangeNotifier
  module Helper
    def target_time_entry(issue)
      last_time_entry = issue.time_entries.last
      return nil if last_time_entry.nil?

      diff = (issue.updated_on - last_time_entry.updated_on).abs
      return nil if diff > 1

      last_time_entry
    end
  end

  Mailer.helper(Helper)

  class TimeEntryChangeNotifyListener < Redmine::Hook::ViewListener
    render_on :view_mailer_issue_edit_after_details,
              partial: "time_entry_change_notifier/show_time_entry"
  end
end
